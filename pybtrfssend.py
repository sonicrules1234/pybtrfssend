import socket, ssl, struct, getpass, sys, traceback, hashlib
import subprocess, shlex, time, glob, re, os.path
# outformat : byte 1 is type of data to be sent -- byte 2 is what buffer size to use
# outformat : byte 3 is padding -- byte4 is length until next info


#        with subprocess.Popen(shlex.split("cat dogs.txt"), stdout=subprocess.PIPE) as p :
#            buf = p.read(bufsize)
class PyBtrfsSend() :
    def __init__(self, hostname, port, certpath, server=False, buffersize=512) :
        self.protocolversion = bytes("pybtrfssend_v1.1.2", "utf-8")

        self.certpath = certpath
        if server == False :
            self.outc = ["subvoltodelete", "auth", "btrfsdata", "info", "donesending", "remotepath", "password", "capabilities", "protocolversion", "deletesnapshot"] 
            self.inc = ["ready", "disconnecting", "error", "success", "tryagain", "askpassword", "protocolversion", "info"]
        else :
            self.inc = ["subvoltodelete", "auth", "btrfsdata", "info", "donesending", "remotepath", "password", "capabilities", "protocolversion", "deletesnapshot"]
            self.outc = ["ready", "disconnecting", "error", "success", "tryagain", "askpassword", "protocolversion", "info"]
        self.outf = self.dictize(self.outc)
        self.inf = self.dictize(self.inc)
        self.instruct = struct.Struct("!BL")
        self.outstruct = struct.Struct("!BL")
        self.hostname = hostname
        #self.protocolstruct = struct.Struct("!H")
        self.port = port
        self.buffersize = buffersize
        self.buffer = b""
        #if server == False :
        #    self.startclient()
        #else :
        #    self.startserver()
    def findlatestsnapshot(self) :
        #rootsubvolume = self.rootsubvolume
        #if rootsubvolume.endswith("/") :
        #    rootsubvolume = rootsubvolume[:-1]
        possibles = glob.glob(self.rootsubvolume + "_*")
        numbers = []
        reobj = re.compile(r"^" + self.rootsubvolume + r"_(\d*)$")
        for possible in possibles :
            numbers.append(int(reobj.search(possible).groups()[0]))
        if numbers == [] :
            numbers.append(-1)
        self.latestsnapshotnum = max(numbers)
        if self.latestsnapshotnum == -1 :
            return self.rootsubvolume
        return self.rootsubvolume + "_" + str(self.latestsnapshotnum)
    def generatenextsnapshot(self) :
        #rootsubvolume = self.rootsubvolume
        #if rootsubvolume.endswith("/") :
        #    rootsubvolume = rootsubvolume[:-1]
        return self.rootsubvolume + "_" + str(self.latestsnapshotnum + 1)
    def dictize(self, inlist) :
        outdict = {}
        for x in range(len(inlist)) :
            outdict[inlist[x]] = x
        return outdict
    def send(self, messagetype, data=b"") :
        #print("sending")
        self.ssock.sendall(self.createinfopack(messagetype, data))
        #print("sent")
        if data != b"" :
            self.ssock.sendall(data)
    def connectandauth(self) :
        print("Connecting")
        self.sock = socket.create_connection((self.hostname, self.port))
        self.context = ssl.create_default_context()
        self.context.check_hostname = False
        self.context.verify_mode = ssl.CERT_NONE
        #self.context.load_verify_locations(self.certpath)
        self.ssock = self.context.wrap_socket(self.sock) #, server_hostname=self.hostname)
        while self.recvinfopackanddata()["messagetype"] != "ready" :
            time.sleep(0.1)
        print("sending protocol version")
        self.send("protocolversion", self.protocolversion)
        #indata = self.getdata(5)
        #inplength = self.protocolstruct.unpack(indata)[0]
        data = self.recvinfopackanddata()
        if data["data"] == self.protocolversion :
            #indata = self.getdata(5)
            #if self.inf[indata[0]] == "askpassword" :
            print("Authing...")
            self.send("auth")
            successful = False
            data = self.recvinfopackanddata()
            if data["messagetype"] == "askpassword" :
                while not successful :
                    password = bytes(getpass.getpass(), "utf-8")
                    self.send("password", password)
                    data = self.recvinfopackanddata()
                    if data["messagetype"] == "success" :
                        return True
                    elif data["messagetype"] == "tryagain" :
                        successful = False
                    else :
                        print("Too many password retries!")
                        return False
        print("Protocol versions don't match!")
        return False
    def startclient(self, rootsubvolume, remotepath, partial=True, deletesnapshot=True) :
        self.deletesnapshot = deletesnapshot
        #self.context = ssl.create_default_context()
        #self.context.load_verify_locations(self.certpath)
        #self.context.load_cert_chain(certfile=certpath, keyfile=keypath)
        self.partial = partial
        self.rootsubvolume = rootsubvolume
        if self.rootsubvolume.endswith("/") :
            self.rootsubvolume = self.rootsubvolume[:-1]
        self.latestsnapshot = self.findlatestsnapshot()
        self.nextsnapshot = self.generatenextsnapshot()
        self.remotepath = remotepath
        if self.remotepath.endswith("/") :
            self.remotepath = self.remotepath[:-1]
        if self.connectandauth() :
            #print("Creating read-only snapshot of " + self.rootsubvolume)
            subprocess.run(shlex.split("btrfs subvolume snapshot -r " + self.rootsubvolume + " " + self.nextsnapshot))
            if self.startdatatransfer() :
                print("Done sending")
                print("Disconnecting")
                try :
                    self.ssock.close()
                    self.sock.close()
                except :
                    traceback.print_exc()
                if self.deletesnapshot:
                    if self.partial :
                        subprocess.run(shlex.split("btrfs subvolume delete " + self.latestsnapshot))
                    else :
                        subprocess.run(shlex.split("btrfs subvolume delete " + self.nextsnapshot))
                #print("Deleted " + self.latestsnapshot)
            else :
                print("There was a problem transferring data.")
        print("Finished")
    def startserver(self, keypath, sha512pass) :
        self.context = ssl.SSLContext()
        self.context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
        #self.context.load_verify_locations(self.certpath)
        self.context.load_cert_chain(certfile=self.certpath, keyfile=keypath)
        self.bindsocket = socket.socket()
        self.bindsocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.bindsocket.bind((self.hostname, self.port))
        self.bindsocket.listen(1)
        self.accepting = True
        try :
            while self.accepting :
                self.sock, fromaddr = self.bindsocket.accept()
                print("Accepted socket")
                self.ssock = self.context.wrap_socket(self.sock, server_side=True)
                print("Wrapped socket")
                self.send("ready")
                infodata = self.recvinfopackanddata()
                #print(infodata)
                if infodata["messagetype"] == "protocolversion" :
                    print("Received protocol version")
                    if infodata["data"] == self.protocolversion :
                        print("Protocol version matched, sending reply")
                        self.send("protocolversion", self.protocolversion)
                        infodata = self.recvinfopackanddata()
                        if infodata["messagetype"] == "auth" :
                            self.send("askpassword")
                        #infodata = self.recvinfopackanddata()
                        goodpass = False
                        while not goodpass :
                            infodata = self.recvinfopackanddata()
                            if infodata["messagetype"] == "password" :
                                h = hashlib.new("sha512")
                                h.update(infodata["data"])
                                if h.hexdigest() == sha512pass :
                                    goodpass = True
                                    self.send("success")
                                else :
                                    self.send("tryagain")
                            else :
                                self.send("error", bytes("Wasn't right messagetype", "utf-8"))
                        infodata = self.recvinfopackanddata()
                        if infodata["messagetype"] == "remotepath" :
                            self.remotepath = infodata["data"].decode("utf-8")
                            self.send("success")
                            infodata = self.recvinfopackanddata()
                            if infodata["messagetype"] == "deletesnapshot" :
                                if infodata["data"].decode() == "1" :
                                    self.deletesnapshot = True
                                else :
                                    self.deletesnapshot = False
                                self.send("success")
                            if self.deletesnapshot :
                                infodata = self.recvinfopackanddata()
                                if infodata["messagetype"] == "subvoltodelete" :
                                    self.subvoltodelete = infodata["data"].decode("utf-8")
                                    self.send("success")
                            infodata = self.recvinfopackanddata()
                            with subprocess.Popen(shlex.split("btrfs receive " + self.remotepath), stdin=subprocess.PIPE) as p : 
                                while infodata["messagetype"] != "donesending" :
                                    if infodata["messagetype"] == "btrfsdata" :
                                        #print(infodata["data"])
                                        p.stdin.write(infodata["data"])
                                        self.send("success")
                                    else :
                                        print(infodata["messagetype"])
                                        self.send("tryagain")
                                    infodata = self.recvinfopackanddata()
                            self.send("disconnecting")
                            self.ssock.shutdown(socket.SHUT_RDWR)
                            self.ssock.close()
                            print("Finished receive")
                            if self.deletesnapshot :
                                subvoltodelete = self.remotepath + "/" + self.subvoltodelete
                                subprocess.run(shlex.split("btrfs subvolume delete " + subvoltodelete))
                                #print("Deleted " + subvoltodelete)
            #self.accepting = False
        finally :
            self.bindsocket.close()
            print("Done!")        
    def startdatatransfer(self) :
        if self.partial :
            btrfscommand = "btrfs send -p " + self.latestsnapshot + " " + self.nextsnapshot
        else :
            btrfscommand = "btrfs send " + self.latestsnapshot
        print("Sending remote path")
        if self.deletesnapshot :
            deletesnapshotvalue = bytes("1", "utf-8")
        else :
            deletesnapshotvalue = bytes("0", "utf-8")
        self.send("remotepath", bytes(self.remotepath, "utf-8"))
        if self.recvinfopackanddata()["messagetype"] == "success" :
            self.send("deletesnapshot", deletesnapshotvalue)
            if self.recvinfopackanddata()["messagetype"] == "success" :
                if self.deletesnapshot :
                    if self.partial :
                        self.send("subvoltodelete", bytes(self.latestsnapshot.split("/")[-1], "utf-8"))
                    else :
                        self.send("subvoltodelete", bytes(self.nextsnapshot.split("/")[-1], "utf-8"))
                if self.recvinfopackanddata()["messagetype"] == "success" : 
                    with subprocess.Popen(shlex.split(btrfscommand), stdout=subprocess.PIPE) as p :
                        chunk = p.stdout.read(self.buffersize)
                        while chunk != b"" :
                            successful = False
                            while not successful :
                                self.send("btrfsdata", chunk)
                                infodata = self.recvinfopackanddata()
                                if infodata["messagetype"] == "success" :
                                    successful = True
                                else :
                                    print("Something bad happened.")
                                    print(infodata["messagetype"])
                                    return False
                            chunk = p.stdout.read(self.buffersize)
                    self.send("donesending")
        if self.recvinfopackanddata()["messagetype"] == "disconnecting" :
            print("Received disconnect message")
            return True
        else :
            return False

    def recvinfopackanddata(self) :
        ininfodata = self.instruct.unpack(self.getdata(5))
        #print(self.inc[ininfodata[0]])
        if ininfodata[1] == 0 :
            data = ""
        else :
            data = self.getdata(ininfodata[1])
        #print(self.inc[ininfodata[0]])
        return {"messagetype":self.inc[ininfodata[0]], "data":data}
    def createinfopack(self, datatype, data=b"") :
        return self.outstruct.pack(self.outf[datatype], len(data))
    def getdata(self, datalength) :
        #print("Receiving data")
        if datalength == 0 :
            return b""
        while len(self.buffer) < datalength :
            #print("inloop")
            self.buffer += self.ssock.recv(self.buffersize)
            #print(self.buffer)

        data = self.buffer[:datalength]
        self.buffer = self.buffer[datalength:]
        #print(str(len(data)))
        return data
