import pybtrfssend
certfile = "/home/westly/pybtrfssend/cert.crt"
keyfile = "/home/westly/pybtrfssend/key.key"
port = 4005
hostname = "192.168.0.12"
buffersize = 4096
sha512pass = "7194b3ce8f51df419f5e796fc84cf9ccfa5a737fbff54d2f601e4d31d41c26c2234486fe7065fbdeb5c6ed0ea6ddb0e07b8cb68f161c1cc553def94a5e1192cf"
x = pybtrfssend.PyBtrfsSend(hostname, port, certfile, server=True, buffersize=buffersize)
x.startserver(keyfile, sha512pass)
